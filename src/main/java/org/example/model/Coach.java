package org.example.model;

public interface Coach {
    String getDailyWorkout();
}
